class InsertError(Exception):
    types = {
        str: "string",
        int: "integer",
        float: "float",
        list: "list",
        dict: "dictionary",
        bool: "boolean"
    }

    def __init__(self, name, email):
        self.message = {
            "wrong fields":[{
                "name": self.types[type(name)]
            },
            {
                "email": self.types[type(email)]
            }]
        }
        super().__init__(self.message)

class EmailError(Exception):
    def __init__(self) -> None:
        self.message = {
            "error": "invalid email, user already exists"
        }
        super().__init__(self.message)
