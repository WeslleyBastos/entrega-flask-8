from logging import error
from flask import Flask, request, jsonify
from os import environ, makedirs, walk, path
from json import load, dump
from app.exceptions import EmailError, InsertError
import os
from environs import Env


app = Flask(__name__)
env = Env()
env.read_env()

FILES_DIRECTORY = environ.get('FILES_DIRECTORY')

if not os.path.exists(FILES_DIRECTORY):
    os.makedirs(FILES_DIRECTORY)

app.get ('/')
def home():
    return "Hey guys! In order to get the database or insert some new user, use '/user' on your main path! Cheers!"


@app.get('/user')
def get_database():
    for _,_, files in walk(FILES_DIRECTORY):
        if not 'database.json' in files:
            with open(f'{FILES_DIRECTORY}/database.json', 'w') as json_file:
                dump({"data":[]}, json_file)
    with open (f'{FILES_DIRECTORY}/database.json', 'r') as json_file:
        data = load(json_file)
        return data, 200


@app.post('/user')
def save_database():
    req = request.get_json()
    name = req["name"]
    email = req["email"]

    try:
        if type(name) != str or type(email) != str:
            raise InsertError(name. email)
    
        name = name.title()
        email = email.lower()

        body_user = {
            "name": name,
            "email": email,
            "id": 1
        }

        for _,_, files in walk(FILES_DIRECTORY):
            if not 'database.json' in files:
                with open (f'{FILES_DIRECTORY}/database.json', 'w') as json_file:
                    dump({"data":[]}, json_file)
                    json_file.close()

        with open (f'{FILES_DIRECTORY}/database.json', 'r') as json_file:
            data = load(json_file)
            centerData = data["data"]
            print(centerData)   
        for user in centerData:
            if user ["email"] == email:
                raise EmailError

            if len(centerData) > 0:
                current_id = data["data"][-1]["id"]
                body_user["id"] = current_id + 1
            
        centerData.append(body_user)
        print(centerData)
        with open (f'{FILES_DIRECTORY}/database.json', 'w') as json_file:
            dump({"data": centerData}, json_file, indent=4)

        centerData = {"data": body_user}

        return jsonify(body_user), 201

    except InsertError as error:
        return error.message, 400

    except EmailError as error:
        return error.message, 400